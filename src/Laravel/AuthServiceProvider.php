<?php

namespace Plus\Laravel;

use Illuminate\Support\Facades\Auth;
use Illuminate\Support\ServiceProvider;

class AuthServiceProvider extends ServiceProvider
{
    public function boot()
    {
        Auth::extend('auth-service', function ($app, $name, array $config) {
            return new AuthServiceGuard;
        });
    }
}
